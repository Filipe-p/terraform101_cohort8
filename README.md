# Terraform 101 :taco:

Terraform is part of IAC (infrastructure as code) and is a Orchestration Management tool.

What does that mean?

It means you can declarativly write code that defines your infrastructure. 


**One cool thing is Terraform maintains state!**
So it knows which resource it has create or not.


### IAC - Infrastrcutre as code 


**Configuration Management tools** These setup machines and configure them internally.

Example software:
- Puppet (ruby)
- Chef (ruby)
- Ansible (We did this one) 
- shell 


**Orchestration Management tools** These setup networks and can orchestrate de deployment of machine images. You can setup a VPC, NACLs, SG, subnets, routes, and launch instances and run init scripts.

Example SW:
- Terraform
- Cloudformation (AWS)


### Objective

- Install terraform
- Launching an EC2
- Define SG
- How to launch a VPC


### Where does it fit with Devops 

Terraform is used to setup the  infrastructure from scratch. Can be a backup option in case something stops working.

In CI/CD it can be used to change the AMI and maintain the infrastructure setup.


### Main sections of Terraform 

- Providers
- Resources
- Modules & Variables

### :police_car::police_car: Key Management & Authentication :police_car::police_car:

Similarly to Hashicorp Packer, terraform used the AWS or other cloud provider (such as Google cloud or Azure) CLI and API keys. So we need to connect and authenticate.

Possible ways:

- API Keys and secretes (we set these in the environment variables)
- Assigning roles to machines



### Terraform setup

You need to install it.

We need to authenthicate. In our Ansible-Packer machine we already have the keys in the environment Variables.


Install guide: https://learn.hashicorp.com/tutorials/terraform/install-cli

```bash

sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform

```

### Providers 

You need to specify a provider and have an authenthication..

We will use AWS provider.

```terraform

provider "aws" {
  region  = "eu-west-1"
  # access_key = "interpolate from env"
  # secret_key = "interpolate from env"
  # Or have them set in the environment and let terraform go the the keys itself
}
```


### Resources

This is the setion where you define resources to be creates

- Resource: aws_instance
- SG
- NACL
- VPCS 
- other

##### aws_instance 

This resource allows you to make a ec2 from an ami.
You can define things suchs as processor type, tags, subnet id, ip and other.

```terraform

resource "aws_instance" "web" {
    ami = "ami-036c52349fb77f0cd"
    instance_type = "t2.micro"
    subnet_id = "subnet-a94474e1"
    associate_public_ip_address = true
    security_groups = ["sg-09ad2e3ed5b7a6ddd", "sg-0cdc70d7952302227"]
    tags = {
      "Name" = "cohor8-filipe-web-tf-launched-ec2"
    }

}
```


##### aws_security_group

This allows to make a security group.


### Referencing in Terraform 

Internal referencing works by calling resouces:
`what_resource.resouce_name.attribute`

### Running Terraform

1. First we need `terraform init`
   1. will install terraform pluggings for specific providers
   2. will start keeping track of state
2. Then you can `terraform validate`
3. Next we `terraform plan` - compares existing infrastructure VS your new declared code.
4. Terraform `terraform apply`
5. check your existing infrastructure with `terraform refresh`

