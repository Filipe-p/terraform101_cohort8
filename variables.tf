# Declare variable here to be used in infrastructure

variable "identity_me_plus_cohort" {
    description = "this is a variable used in setting the names of resources in aws. Identifies user and what cohort"
    default = "cohor8-fp-tf"
}