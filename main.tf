# Providers 
## Defines where to create resources and possibly deploy things

provider "aws" {
  region  = "eu-west-1"
  # access_key = "interpolate from env"
  # secret_key = "interpolate from env"
  # Or have them set in the environment and let terraform go the the keys itself
}


# Resource aws-security group
## We need to find the right resource, name it and then follow the documentation

### Creates new SG
resource "aws_security_group" "sg_webapp" {
    description = "This security group will allow traffic on port 80 an 443 to the world"
    # vpc_id = "vpc-4bb64132"
    vpc_id = aws_vpc.filipe-main-vpc.id
    # rules in (ingress) and out (egress)

    #outbound
    egress {
           from_port    = 0
           to_port      = 0
           protocol     = -1
           cidr_blocks      = ["0.0.0.0/0"]
           ipv6_cidr_blocks = ["::/0"]
        }

    #allow port 80 

    #Inbound
    ingress {
           from_port    = 80
           to_port      = 80
           protocol     = "tcp"
           cidr_blocks      = ["0.0.0.0/0"]
           ipv6_cidr_blocks = ["::/0"]
           description  = "Allow port 80 to world"
        }
    #allow por 443
    ingress {
           from_port    = 443
           to_port      = 443
           protocol     = "tcp"
           cidr_blocks      = ["0.0.0.0/0"]
           ipv6_cidr_blocks = ["::/0"]
           description  = "Allow port 443 to world"
        }
    tags = {
      "Name" = "${var.identity_me_plus_cohort}-security-group"
    }
}

resource "aws_security_group" "sg_webapp_allow_port_ssh" {
    description = "This security group will allow traffic on port 22 from my ip"
    # vpc_id = "vpc-4bb64132"
    vpc_id = aws_vpc.filipe-main-vpc.id
    # rules in (ingress) and out (egress)

    egress {
           from_port    = 0
           to_port      = 0
           protocol     = -1
           cidr_blocks      = ["0.0.0.0/0"]
           ipv6_cidr_blocks = ["::/0"]
        }

    #allow port 22
    ingress {
           from_port    = 22
           to_port      = 22
           protocol     = "tcp"
           cidr_blocks      = ["94.63.111.67/32"]
           description  = "Allow port 22 to my"
        }

    tags = {
      "Name" = "${var.identity_me_plus_cohort}-group-port22"
    }
}

# Create A VPC (will have an id)

resource "aws_vpc" "filipe-main-vpc" {
  cidr_block       = "12.0.0.0/16"
  tags = {
    Name = "${var.identity_me_plus_cohort}-vpc"
  }
}

# Create IGW (attched to vpc)

resource "aws_internet_gateway" "gw" {
   vpc_id = aws_vpc.filipe-main-vpc.id
# vpc_id = "vpc-0e5375b6bff1066e1"

  tags = {
    Name = "${var.identity_me_plus_cohort}-igw"
  }
}

# Create Subnet (needs vpc.id)
resource "aws_subnet" "public-sub" {
  vpc_id     = aws_vpc.filipe-main-vpc.id
  cidr_block = "12.0.1.0/24"

  tags = {
    Name = "${var.identity_me_plus_cohort}-public-sub"
  }
}


# Create NACL (need vpc.id and subnet.id)
resource "aws_network_acl" "main" {
  vpc_id = aws_vpc.filipe-main-vpc.id

  egress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
    }

  ingress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
    }
  

  tags = {
    Name = "${var.identity_me_plus_cohort}-nacl"
  }
}


# Create Routes (need subnet id, and igw.id)
# Creating a route table
resource "aws_route_table" "routepublic" {
  vpc_id = aws_vpc.filipe-main-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name = "${var.identity_me_plus_cohort}route.public"
  }
}

# Route table associations
resource "aws_route_table_association" "routeapp" {
  subnet_id = aws_subnet.public-sub.id
  route_table_id = aws_route_table.routepublic.id
}

# Create SG 
# Inside all of this add the instance 


# Resources creating an EC2 Instance (Launches new instance)
resource "aws_instance" "web" {
    ami = "ami-036c52349fb77f0cd"
    instance_type = "t2.micro"
    # subnet_id = "subnet-a94474e1"
    subnet_id = aws_subnet.public-sub.id
    associate_public_ip_address = true
    # i want to reference a SG id, but the SG has not yet been create. use internal referencing 
    # security_groups = ["sg-09ad2e3ed5b7a6ddd", "sg-0cdc70d7952302227", aws_security_group.sg_webapp.id, aws_security_group.sg_webapp_allow_port_ssh.id ]
    security_groups = [aws_security_group.sg_webapp.id, aws_security_group.sg_webapp_allow_port_ssh.id ]
    user_data = data.template_file.init.rendered
    tags = {
      "Name" = "${var.identity_me_plus_cohort}-web-launched-ec2"
    }

}


data "template_file" "init" {
    #template = "path/local/machine/template.sh.tpl"
    template = "init.sh.tpl"
    var = {
        example_var = "123.2.2.1"
    }
}



## Pseudo code
## Iteration 1
# One more public subnet in another vailability zone
# Another instance in that new subnet 
# Loadbalancer and Auto sclae group

# Intance with Jenkins AMI
# Intance with Agent Node AMI 

# Private subnet
    # thought out NACL rules 
# RDS (might need a subnet in private?)

# Jenkins 
# As the code evolves, keep the posting and merging new AMI IDs to the Bitbucket so the terraform can keep working with latests code
# Same goes for the folder and state file of Terraform 

## When you run terraform and nothing exists:
# it creates Jenkins, Agente node, VPC, Places Petclinic on Production
# When it runs from jenkins and VPC and other resources exists, it should only change the new AMI - relative tot he Petclinic. 

